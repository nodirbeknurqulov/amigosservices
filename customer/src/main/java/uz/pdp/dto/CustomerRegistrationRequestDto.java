package uz.pdp.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

// Nurkulov Nodirbek 4/15/2022  5:40 PM
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CustomerRegistrationRequestDto {
    private String firstName;

    private String lastName;

    private String email;
}
