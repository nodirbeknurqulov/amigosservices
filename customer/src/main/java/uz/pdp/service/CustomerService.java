package uz.pdp.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import uz.pdp.dto.FraudCheckResponse;
import uz.pdp.entity.Customer;
import uz.pdp.dto.CustomerRegistrationRequestDto;
import uz.pdp.repository.CustomerRepository;

// Nurkulov Nodirbek 4/15/2022  5:54 PM
@Service
@RequiredArgsConstructor
public class CustomerService {

    private final CustomerRepository customerRepository;
    private final RestTemplate restTemplate;

    public void registerCustomer(CustomerRegistrationRequestDto customerRegistrationRequestDto) {
        Customer customer = Customer.builder()
                .firstName(customerRegistrationRequestDto.getFirstName())
                .lastName(customerRegistrationRequestDto.getLastName())
                .email(customerRegistrationRequestDto.getEmail())
                .build();

        //todo: check if email valid
        //todo: check if email not taken
        customerRepository.saveAndFlush(customer);

        //todo: store customer in db --done
        //todo: check if fraudster
        FraudCheckResponse fraudCheckResponse = restTemplate.getForObject(
                "http://localhost:8081/api/v1/fraud-check/{customerId}",
                FraudCheckResponse.class,
                customer.getId()
        );

        assert fraudCheckResponse != null;
        if (fraudCheckResponse.isFraudster()){
            throw new IllegalStateException("fraudster");
        }

        //todo: send notification
    }
}
