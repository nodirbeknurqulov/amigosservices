package uz.pdp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

public interface FraudCheckHistoryRepository extends JpaRepository<uz.pdp.entity.FraudCheckHistory,Integer> {
}
