package uz.pdp.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import uz.pdp.repository.FraudCheckHistoryRepository;

import java.time.LocalDateTime;

// Nurkulov Nodirbek 4/16/2022  12:42 PM
@Service
@AllArgsConstructor
public class FraudCheckHistoryService {

    private final FraudCheckHistoryRepository fraudCheckHistory;

    public boolean isFraudulentCustomer(Integer customerId){
        fraudCheckHistory.save(uz.pdp.entity.FraudCheckHistory
                .builder()
                        .customerId(customerId)
                        .isFraudster(false)
                        .createdAt(LocalDateTime.now())
                .build());
        return false;
    }
}
