package uz.pdp.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

// Nurkulov Nodirbek 4/16/2022  12:52 PM
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FraudCheckResponse {
    private boolean isFraudster;
}
